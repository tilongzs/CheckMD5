#pragma once
#include "MD5.h"
#include "afxwin.h"

class CCheckMD5Dlg : public CDialogEx
{
public:
	CCheckMD5Dlg(CWnd* pParent = NULL);	

#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CHECKMD5_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);

protected:
	HICON m_hIcon;

	CEdit		_editVersion;
	CString		_strVersion;

	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	CString GetCurrectPath();
	void GetFolderDayFile(const CString& pathStr);
	void CheckVersion();
	CString CombinePath(const CString& folder, const CString& extraPath);

	afx_msg void OnBnClickedCheck();
	afx_msg void OnEnChangeVersion();	
public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};

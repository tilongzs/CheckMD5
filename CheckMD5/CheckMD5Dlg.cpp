#include "stdafx.h"
#include "CheckMD5.h"
#include "CheckMD5Dlg.h"
#include "afxdialogex.h"
#include <set>
#include <fstream> 
#include <iostream>
#include <cstringt.h> 
#include "assert.h"  
#include<string.h>
using namespace std;
#include "../jsoncpp/include/json/json.h"
#pragma comment(lib, "jsoncpp.lib")  

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include <locale>
#include <codecvt>
#include <string>
static wstring_convert<codecvt_utf8_utf16<wchar_t>> StrConverter;	// UTF8、UTF16互转

Json::Value jsonFile;
Json::Value jsonVersion;

CCheckMD5Dlg::CCheckMD5Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_CHECKMD5_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCheckMD5Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, _editVersion);
}

BEGIN_MESSAGE_MAP(CCheckMD5Dlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON2, &CCheckMD5Dlg::OnBnClickedCheck)
	ON_EN_CHANGE(IDC_EDIT1, &CCheckMD5Dlg::OnEnChangeVersion)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

BOOL CCheckMD5Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	SetIcon(m_hIcon, TRUE);		
	SetIcon(m_hIcon, FALSE);	

	_editVersion.SetFocus();
	return TRUE;
}

void CCheckMD5Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this);

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

HCURSOR CCheckMD5Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

CString CCheckMD5Dlg::GetCurrectPath()
{
	WCHAR buf[MAX_PATH];
	HINSTANCE hInstance = AfxGetApp()->m_hInstance;
	GetModuleFileName(NULL, buf, MAX_PATH * sizeof(WCHAR));
	GetLongPathName(buf, buf, MAX_PATH * sizeof(WCHAR));
	PathRemoveFileSpec(buf);
	return buf;
}

void CCheckMD5Dlg::OnBnClickedCheck()
{
	CString jsonPath = CombinePath(GetCurrectPath(), L"pathmd5.json");
	DeleteFile(jsonPath); // 删除旧文件

	wofstream os;
	os.open(jsonPath);
	GetFolderDayFile(GetCurrectPath());
	Json::StyledWriter sw;
	os << sw.write(jsonFile).c_str();
	os.close();

	CheckVersion();
	MessageBox(TEXT("完成"));
	EndDialog(0);
}

void CCheckMD5Dlg::GetFolderDayFile(const CString& pathStr)
{
	CString myDataPath, fdPath;
	myDataPath = CombinePath(pathStr, L"*.*");
	jsonFile["version"] = Json::Value(_strVersion);

	WCHAR filename[MAX_PATH] = { 0 };

	// 排除制定的扩展名
	std::set<CString> abandonExtensions;
	abandonExtensions.insert(L".cfg");
	abandonExtensions.insert(L".json");

	CFileFind find;
	BOOL bf = find.FindFile(myDataPath);
	while (bf)
	{
	    bf = find.FindNextFile();
		if (!find.IsDots())
		{
			fdPath = find.GetFilePath();
			if (find.IsDirectory())
			{
				GetFolderDayFile(fdPath);
			}
			else
			{
				// 排除制定扩展名的文件
				if (abandonExtensions.find(CString(PathFindExtension(fdPath)).MakeLower()) != abandonExtensions.end())
				{
					continue;
				}

				// 排除指定文件
				ZeroMemory(filename, MAX_PATH);
				wcscpy_s(filename, fdPath.GetBuffer());
				PathStripPath(filename);
				if (CString(filename).MakeLower() == CString(L"checkmd5.exe"))
				{
					continue;
				}

				std::ifstream handle;
				handle.open(fdPath, std::ios::binary);
				CMD5 md5(handle);
				CString localMD5(md5.toString().c_str());
				CString relativePath = fdPath.Right(fdPath.GetLength() - GetCurrectPath().GetLength() - 1); // 相对路径+文件名
				Json::Value files;
				files["MD5"] = StrConverter.to_bytes(localMD5);
				files["Path"] = StrConverter.to_bytes(relativePath);
				jsonFile["files"].append(files);
			}
		}
	}
	find.Close();
}

void CCheckMD5Dlg::OnEnChangeVersion()
{
	_editVersion.GetWindowText(_strVersion); 
}

void CCheckMD5Dlg::CheckVersion()
{
	wofstream os;
	CString jsonPath = CombinePath(GetCurrectPath(), L"version.json");
    DeleteFile(jsonPath); // 删除旧文件

	Json::Value version;
	jsonVersion["version"] = StrConverter.to_bytes(_strVersion);
	Json::StyledWriter sy;
	os.open(jsonPath);
	os << sy.write(jsonVersion).c_str();
	os.close();
}

CString CCheckMD5Dlg::CombinePath(const CString& folder, const CString& extraPath)
{
	WCHAR buf[MAX_PATH];
	wcscpy_s(buf, folder);
	PathAppend(buf, extraPath);
	return buf;
}

void CCheckMD5Dlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	_editVersion.SetFocus();
}
